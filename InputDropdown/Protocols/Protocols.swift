//
//  Protocols.swift
//  InputDropdown
//
//  Created by Amadeu Real on 10/08/17.
//  Copyright © 2017 Amadeu Real. All rights reserved.
//

protocol InputDropdownDataSource{
	func displayElements(_ element: InputDropdown) -> [String]
}

protocol InputDropdownDelegate {
	func didSelect(_ item: InputDropdown, _ val: String, _ index: Int)
	func didPickDate(_ date: Date)
}

protocol InputDropdownInside {
	func didSelect(_ item: ListItemDropdown)
}

extension InputDropdownDelegate {
	
	func didPickDate(_ date: Date){}
	
	func didSelect(_ item: InputDropdown, _ val: String, _ index: Int){}
	
}

