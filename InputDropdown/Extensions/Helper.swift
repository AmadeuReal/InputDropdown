//
//  Helper.swift
//  InputDropdown
//
//  Created by Amadeu Real on 10/08/17.
//  Copyright © 2017 Amadeu Real. All rights reserved.
//

import UIKit

struct Dimension {
	
	static var eSize: CGSize {
		return UIScreen.main.bounds.size
	}
	
	let left: CGFloat
	let right: CGFloat
	let top: CGFloat
	let bottom: CGFloat
	init(left: CGFloat, top: CGFloat, right: CGFloat, bottom: CGFloat) {
		self.left = left
		self.top = top
		self.right = right
		self.bottom = bottom
	}
	
	init(cima: CGFloat, lados: CGFloat) {
		
		self.left = lados
		self.top = cima
		self.right = lados
		self.bottom = cima
	}
	
	init(_ dim: CGFloat) {
		
		self.left = dim
		self.top = dim
		self.right = dim
		self.bottom = dim
	}
	
	func toPoint() -> CGPoint {
		return CGPoint(x: left, y: top)
	}
	
	func getLateralPadding() -> CGFloat {
		return left + right
	}
	
	func getTopAndBottomPadding() -> CGFloat {
		return bottom + top
	}
}

extension UIColor {
	
	var mainColor: UIColor {
		return UIColor(colorWithHexValue: 0xbba45b)
	}
	
	var textColor: UIColor {
		return UIColor(colorWithHexValue: 0x000)
	}
	
	convenience init(red: Int, green: Int, blue: Int) {
		assert(red >= 0 && red <= 255, "Invalid red component")
		assert(green >= 0 && green <= 255, "Invalid green component")
		assert(blue >= 0 && blue <= 255, "Invalid blue component")
		
		self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
	}
	
	convenience init(colorWithHexValue value: Int, alpha:CGFloat = 1.0){
		self.init(
			red: CGFloat((value & 0xFF0000) >> 16) / 255.0,
			green: CGFloat((value & 0x00FF00) >> 8) / 255.0,
			blue: CGFloat(value & 0x0000FF) / 255.0,
			alpha: alpha
		)
	}
	
	convenience init(netHex:Int) {
		self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
	}
}

extension UIView {
	var position: (height: CGFloat, width: CGFloat) {
		return (height: self.frame.origin.y + self.frame.height, width: self.frame.origin.x + self.frame.width)
	}
}

extension UIViewController {
	
	func generateTitleView(withTitle title: String, withBottomLabel bottomText: String? , withColor color: UIColor?) {
		let defaultFont = UIFont(name: "HelveticaNeue", size: 16)!
		let centerAligment = NSMutableParagraphStyle()
		centerAligment.alignment = .center
		
		let mColor = color ?? UIColor().mainColor
		
		let myAttribute = [ NSAttributedStringKey.font: defaultFont, NSAttributedStringKey.foregroundColor: mColor, NSAttributedStringKey.paragraphStyle:  centerAligment]
		let myString = NSMutableAttributedString(string: title.uppercased(), attributes: myAttribute )
		
		if let text = bottomText {
			
			let attrString = NSAttributedString(string: "\n"+text)
			myString.append(attrString)
			let length = text.characters.count + 1
			let myRange = NSRange(location: title.characters.count, length: length)
			myString.addAttributes([NSAttributedStringKey.font : defaultFont.withSize(12), NSAttributedStringKey.foregroundColor: mColor, NSAttributedStringKey.paragraphStyle:  centerAligment], range: myRange)
			
		}
		
		let titleView = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: 64, height: 64)))
		titleView.numberOfLines = 0
		titleView.attributedText = myString
		
		navigationItem.titleView = titleView
	}
	
	func addBackImage(withImage image: UIImage) {
		navigationItem.hidesBackButton = true
		navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .done, target: self, action: nil)
	}
	
}


extension Bundle {
	
	static func loadView<T>(fromNib name: String, withType type: T.Type) -> T {
		
		guard let view = Bundle.main.loadNibNamed(name, owner: nil, options: nil)?.first as? T else {
			fatalError("Não foi encontrado nenhum ficheiro com esse nome.")
		}
		
		return view
	}
}

extension UITextField {
	
	func alterarPlaceholder (_ string: String, _ color: UIColor) {
		
		attributedPlaceholder = NSAttributedString(string: string)
		placeholderColor = color
		
	}
	
	@IBInspectable var placeholderColor: UIColor {
		get {
			guard let currentAttributedPlaceholderColor = attributedPlaceholder?.attribute(NSAttributedStringKey.foregroundColor, at: 0, effectiveRange: nil) as? UIColor else { return UIColor.clear }
			return currentAttributedPlaceholderColor
		}
		set {
			guard let currentAttributedString = attributedPlaceholder else { return }
			let attributes = [NSAttributedStringKey.foregroundColor : newValue]
			
			attributedPlaceholder = NSAttributedString(string: currentAttributedString.string, attributes: attributes)
		}
	}
}

func delay(_ delay:Double, closure:@escaping  () -> () ) {
	DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay, execute: closure)
}
func Main(_ function: @escaping () -> () ) {
	DispatchQueue.main.async(execute: function)
}

func Background(_ function: @escaping  () -> () ) {
	DispatchQueue.global(qos: .background).async(execute: function)
}


