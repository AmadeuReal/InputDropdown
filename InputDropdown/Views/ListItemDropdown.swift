//
//  ListItemDropdown.swift
//  InputDropdown
//
//  Created by Amadeu Real on 10/08/17.
//  Copyright © 2017 Amadeu Real. All rights reserved.
//

class ListItemDropdown: UIView {
	
	open var delegate: InputDropdownInside?
	
	fileprivate var status: Bool 	= false
	fileprivate var title: String 	= ""
	fileprivate var index: Int 		= 0
	fileprivate var size: CGSize 	= CGSize()
	
	static var color: UIColor 		= .white
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override init(frame: CGRect) {
		
		super.init(frame: frame)
		setupViews()
		
	}
	
	init(size: CGSize, title: String, index: Int) {
		super.init(frame: CGRect(origin: .zero, size: size))
		
		frame.size.height = 0
		alpha = 0
		
		self.title = title
		self.size = size
		self.index = index
		
		addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didClick)))
	}
	
	func setupViews() {
		
		addSubview(titleLabel)
		addSubview(line)
	}
	
	lazy var titleLabel: UILabel = {
		let label = UILabel(frame: CGRect(origin: .zero, size: self.size))
		
		label.frame.origin.x = 8
		label.textAlignment = .left
		label.text = self.title.uppercased()
		label.textColor = ListItemDropdown.color
		
		return label
	}()
	
	
	lazy var line: UIView = {
		let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.size.width - 16, height: 1)))
		view.backgroundColor = UIColor().mainColor
		view.frame.origin.y = self.size.height - 1
		view.frame.origin.x = 8
		return view
	}()
	
	func isLast() {
		line.removeFromSuperview()
	}
	
	func toggle() {
		defer {status = !status}
		UIView.animate(withDuration: 0.2) {
			self.frame.size.height = self.status ? 0 : self.size.height
			self.alpha = self.status ? 0 : 1
		}
	}
	
	@objc func didClick() {
		delegate?.didSelect(self)
	}
	
	open func getDados() -> (title: String, index: Int) {
		return (title, index)
	}
	
}
