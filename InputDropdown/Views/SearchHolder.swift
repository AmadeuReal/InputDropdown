//
//  SearchHolder.swift
//  selfit
//
//  Created by Amadeu Real on 12/06/17.
//  Copyright © 2017 Amadeu Real. All rights reserved.
//

import UIKit


class SearchHolder: UIView {
	
	open var padding: CGFloat 	= 8
	open var color: UIColor		= UIColor(colorWithHexValue: 0x454343)
	open var text: String 		= ""
	
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	func setup() {
		addSubview(search)
	}
	
	open func setPlaceholder(_ placeholder: String) {
		
		search.alterarPlaceholder(placeholder.uppercased(), color)
		text = placeholder
		
	}
	open func setBackgroundColor(with newColor: UIColor){
		search.backgroundColor = newColor
	}
	
	open func setTextColor(with newColor: UIColor){
		search.textColor = newColor
	}
	
	lazy var search: SublimeTextField = {
		
		let field = SublimeTextField(frame: self.frame)
		field.backgroundColor = .white
		
		let font = field.font ?? UIFont(name: "HelveticaNeue", size: 16)!
		field.font = font.withSize(self.frame.height/2)
		field.textColor = self.color
		
		return field
	}()
	
	open func addImageRight(_ imagem: UIImage) {
		
		let image = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 25, height: 25)))
		let view =  UIView(frame: CGRect(x: 0, y: 0, width: 25 + padding, height: 20))
		image.image = imagem
		view.addSubview(image)
		search.rightView = view
		
		search.rightViewMode = .always
		
	}
	
	
	open func changeFont(_ font: UIFont){
		search.font = font
	}
	
	open func changeBackgroundColor( _ color: UIColor) {
		backgroundColor = color
	}
	open func changetextBackGroundColor(_ color: UIColor){
		search.backgroundColor = color
		backgroundColor = color
	}
}

class SublimeTextField: UITextField {
	
	public var textFieldInsets = CGPoint(x: 12, y: -18)
	
	open override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
		return textRect(forBounds: bounds)
	}
	
	open override func editingRect(forBounds bounds: CGRect) -> CGRect {
		return textRect(forBounds: bounds)
	}
	
	override open func textRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.offsetBy(dx: textFieldInsets.x, dy: textFieldInsets.y + frame.height / 2)
	}
	
	
}

