//
//  Inputdropdown.swift
//  InputDropdown
//
//  Created by Amadeu Real on 12/06/17.
//  Copyright © 2017 Amadeu Real. All rights reserved.
//

import UIKit

class InputDropdown: SearchHolder {
	
	open var delegate: InputDropdownDelegate?
	open var dataSource: InputDropdownDataSource?
	
	fileprivate var itemColor: UIColor?
	fileprivate var parent: UIView?
	
	fileprivate var isActive: Bool 				= false
	fileprivate var status: Bool 				= false
	fileprivate var parentWillGrow: Bool 		= false
	fileprivate var listSize: CGFloat 			= 0
	fileprivate var buttons: [ListItemDropdown] = []
	fileprivate var initialSize: CGFloat 		= 0
	fileprivate let bottomLayer 				= CALayer()
	
	override var color: UIColor {
		willSet(val){
			search.textColor = val
			search.alterarPlaceholder(text, val)
		}
	}
	
	lazy var dateFormatter: DateFormatter = {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd 'de' MMMM 'de' yyyy"
		return dateFormatter
	}()
	
	lazy var datePicker: UIDatePicker = {
		let datePicker = UIDatePicker()
		datePicker.date = Date()
		datePicker.datePickerMode = .date
		
		let toolbar = UIToolbar()
		toolbar.sizeToFit()
		toolbar.frame.origin.y = datePicker.frame.maxY
		
		let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(self.endEditingDatePicker))
		toolbar.setItems([doneBtn], animated: true)
		
		self.search.inputAccessoryView = toolbar
		return datePicker
	}()
	
	var isDatePicker: Bool = false {
		willSet (val){
			if val {
				search.isUserInteractionEnabled = true
				search.inputView = datePicker
			}
		}
	}
	
	@objc func endEditingDatePicker() {
		
		endEditing(true)
		
		let selectedDate: String = dateFormatter.string(from: datePicker.date)
		
		search.text = selectedDate
		delegate?.didPickDate(datePicker.date)
		
	}
	
	
	func addBottomLine() {
		
		bottomLayer.frame = CGRect(origin: CGPoint(x: 16, y: frame.height - 1), size: CGSize(width: frame.width - 16 * 2, height: 1))
		bottomLayer.borderWidth = 1
		bottomLayer.borderColor = UIColor().mainColor.cgColor
		
		self.layer.addSublayer(bottomLayer)
	}
	
	override func willMove(toSuperview newSuperview: UIView?) {
		super.willMove(toSuperview: newSuperview)
		
		guard let cases = dataSource?.displayElements(self) else {return}
		parent = newSuperview
		
		if isActive {
			populateView(with: cases)
		}
		
	}
	
	open func reloadView(with cases: [String]) {
		
		search.text = nil
		isActive = true
		removeViews()
		populateView(with: cases)
		
	}
	
	private func populateView(with cases: [String]) {
		
		for c in cases.enumerated() {
			let item = ListItemDropdown(size: bounds.size, title: c.element, index: c.offset)
			item.frame.origin.y = CGFloat(c.offset + 1) * initialSize
			
			item.delegate = self
			
			item.backgroundColor = itemColor ?? UIColor(colorWithHexValue: 0x000, alpha: 1)
			
			addSubview(item)
			buttons.append(item)
			
			if c.offset == cases.count - 1 {
				item.isLast()
			}
		}
		
		listSize = CGFloat(cases.count + 1) * initialSize
		
	}
	
	override func setup(){
		super.setup()
		search.isUserInteractionEnabled = false
		initialSize = bounds.size.height
		
		addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toggleStatus)))
		isActive = false
	}
	private func removeViews() {
		
		for v in subviews  {
			if let btn = v as? ListItemDropdown {
				btn.removeFromSuperview()
			}
		}
		
		buttons = []
	}
	
	@objc func toggleStatus() {
		if !isActive {
			return
		}
		
		defer {status = !status}
		
		for view in self.buttons {
			view.toggle()
		}
		
		UIView.animate(withDuration: 0.2) {
			self.frame.size.height = self.status ? self.initialSize : self.listSize
			
			self.bottomLayer.frame.origin.y = self.status ? self.initialSize-1 : self.listSize-1
			if self.parentWillGrow {
				
				self.parent?.frame.size.height = self.status ? self.position.height : self.position.height
			}
		}
		
	}
	
	
}

extension InputDropdown: InputDropdownInside {
	
	
	func didSelect(_ item: ListItemDropdown) {
		let itemDados = item.getDados()
		search.text = itemDados.title
		toggleStatus()
		
		delegate?.didSelect(self, itemDados.title, itemDados.index)
	}
	
}



